# vim: set ft=make sts=4 ts=4 sw=4 noexpandtab fo-=t:

################################################################################
# config
################################################################################
SHELL=/usr/bin/env bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))
$(info current_makefile_dirname_abspath=$(current_makefile_dirname_abspath))
$(info current_makefile_dirname_realpath=$(current_makefile_dirname_realpath))

makefile_name=$(abspath $(lastword $(MAKEFILE_LIST)))
makefile_basename=$(dir $(makefile_name))
makefile_dirname=$(notdir $(makefile_name))

asciidoctor=asciidoctor --trace -r asciidoctor-diagram
asciidoctor_html5=$(asciidoctor) --backend html5
asciidoctor_xhtml5=$(asciidoctor) --backend xhtml5
asciidoctor_exhtml5=$(asciidoctor) --backend xhtml5 --no-header-footer
asciidoctor_html5s=$(asciidoctor) -r asciidoctor-html5s --backend html5s --no-header-footer
#asciidoctor_cfl=$(asciidoctor) --backend backend-cfl
## slim, haml, erb
#asciidoctor_cfl=$(asciidoctor) --backend xhtml5 --template-dir template-cfl-haml --template-engine haml --attribute 'notitle' --attribute 'nofooter'
asciidoctor_cfl=$(asciidoctor) --backend xhtml5 --template-dir template-cfl-erb --template-engine erb --attribute 'notitle' --attribute 'nofooter'
asciidoctor_db5=$(asciidoctor) --backend docbook5
#asciidoctor_pdf=$(asciidoctor) --require asciidoctor-pdf --backend pdf --require asciidoctor-mathematical --attribute mathematical-format=svg
#asciidoctor_pdf=$(asciidoctor) --require asciidoctor-pdf --backend pdf --require ./lib/mathoid-treeprocessor.rb
#asciidoctor_pdf=asciidoctor-pdf --trace --backend pdf --require asciidoctor-mathematical --require asciidoctor-diagram --attribute mathematical-format=svg
asciidoctor_pdf=asciidoctor-pdf --trace --backend pdf --require asciidoctor-diagram --attribute mathematical-format=svg

nwdiag=nwdiag
plantuml=plantuml

build_dir=build

git_version:=$(shell git describe --tags --always)
git_date:=$(shell git --no-pager show --no-patch --format=format:'%ad' --date=format:'%Y-%m-%d' "`git describe --tags --always`" )
git_datetime:=$(shell git --no-pager show --no-patch --format=format:'%ai' --date=format:'%Y-%m-%d' "`git describe --tags --always`" )
git_subject:=$(shell git --no-pager show --no-patch --format=format:'%s' --date=format:'%Y-%m-%d' "`git describe --tags --always`" )
git_commit_count:=$(shell git rev-list --no-merges --count HEAD)
git_local_changes:=$(shell [ -n "$$(git status --porcelain | grep -v '^??')" ] && echo "M")
date_today:=$(shell date "+%Y-%m-%d")
year_today:=$(shell date "+%Y")
datetime_now:=$(shell date "+%Y-%m-%d %H:%M:%S")

################################################################################
# rules
################################################################################

.PHONY: all
all: asciidoc

#metauml_sources = \
#	$(filter-out images/helpers.mp,$(wildcard images/*.mp)) \

#metauml_targets_svg=$(foreach source,$(metauml_sources:.mp=.svg),$(build_dir)/$(source))
#metauml_targets=$(metauml_targets_svg)

#.PHONY: metauml-svg
#metauml-svg: $(metauml_targets_svg)

nwdiag_sources = \
	$(wildcard images/*.nwdiag) \

#$(info nwdiag_sources=$(nwdiag_sources))
nwdiag_targets_svg=$(foreach source,$(nwdiag_sources:.nwdiag=.svg),$(build_dir)/$(source))
nwdiag_targets_png=$(foreach source,$(nwdiag_sources:.nwdiag=.png),$(build_dir)/$(source))
nwdiag_targets=$(nwdiag_targets_svg) $(nwdiag_targets_png)
#$(info nwdiag_targets=$(nwdiag_targets_svg))

.PHONY: nwdiag-png
nwdiag-png: $(nwdiag_targets_png)
.PHONY: nwdiag-svg
nwdiag-svg: $(nwdiag_targets_svg)
.PHONY: nwdiag
nwdiag: $(nwdiag_targets)

plantuml_sources = $(filter-out $(build_dir)/%, \
	$(wildcard images/*.plantuml) \
	)

#$(info plantuml_sources=$(plantuml_sources))
plantuml_targets_svg=$(foreach source,$(plantuml_sources:.plantuml=.svg),$(build_dir)/$(source))
plantuml_targets_png=$(foreach source,$(plantuml_sources:.plantuml=.png),$(build_dir)/$(source))
plantuml_targets=$(plantuml_targets_svg) $(plantuml_targets_png)
#$(info plantuml_targets=$(plantuml_targets_svg))

.PHONY: plantuml-png
plantuml-png: $(plantuml_targets_png)
.PHONY: plantuml-svg
plantuml-svg: $(plantuml_targets_svg)
.PHONY: plantuml
plantuml: $(plantuml_targets)

image_sources = $(filter-out $(build_dir)/%, \
	$(wildcard images/*.svg) \
	$(wildcard images/*.pdf) \
	$(wildcard images/*.png) \
	$(wildcard images/*.jpg) \
	)

#$(info image_sources=$(image_sources))
image_targets_pre = $(foreach source,$(image_sources),$(build_dir)/$(source)) \
	$(foreach source,$(wildcard images/*.pdf),$(build_dir)/$(source:.pdf=-crop.pdf)) \
	$(foreach source,$(wildcard images/*.pdf),$(build_dir)/$(source:.pdf=-crop.svg)) \
	$(foreach source,$(wildcard images/*.pdf),$(build_dir)/$(source:.pdf=-pages)) \

image_targets = \
	$(image_targets_pre) \


#	$(foreach target,$(filter $(build_dir)/images/%.svg,$(plantuml_targets_svg) $(nwdiag_targets_svg) $(image_targets_pre)),$(target:.svg=.wmf)) \
	$(foreach target,$(filter $(build_dir)/images/%.svg,$(plantuml_targets_svg) $(nwdiag_targets_svg) $(image_targets_pre)),$(target:.svg=.emf)) \

$(info image_targets=$(image_targets))

asciidoc_sources = \
	$(filter-out $(build_dir)/% README.adoc,$(wildcard *.adoc) $(wildcard */*.adoc))

$(info asciidoc_sources=$(asciidoc_sources))

asciidoc_targets_html5=$(foreach source,$(asciidoc_sources:.adoc=.html),$(build_dir)/$(source))
asciidoc_targets_xhtml5=$(foreach source,$(asciidoc_sources:.adoc=.xhtml),$(build_dir)/$(source))
asciidoc_targets_exhtml5=$(foreach source,$(asciidoc_sources:.adoc=.emb.xht),$(build_dir)/$(source))
asciidoc_targets_html5s=$(foreach source,$(asciidoc_sources:.adoc=.s.html),$(build_dir)/$(source))
asciidoc_targets_cfl=$(foreach source,$(asciidoc_sources:.adoc=.cfl.xml),$(build_dir)/$(source))
asciidoc_targets_pdf=$(foreach source,$(asciidoc_sources:.adoc=.pdf),$(build_dir)/$(source))
asciidoc_targets_db5=$(foreach source,$(asciidoc_sources:.adoc=.db5.xml),$(build_dir)/$(source))
asciidoc_targets_md=$(foreach source,$(asciidoc_sources:.adoc=.md),$(build_dir)/$(source))
asciidoc_targets=\
	$(asciidoc_targets_html5) \
	$(asciidoc_targets_pdf) \

#
#	$(asciidoc_targets_xhtml5) \
	$(asciidoc_targets_exhtml5) \
	$(asciidoc_targets_cfl) \
	$(asciidoc_targets_db5) \
	$(asciidoc_targets_md) \

$(info asciidoc_targets=$(asciidoc_targets))

.PHONY: asciidoc-html5
asciidoc-html5: $(asciidoc_targets_html5)
.PHONY: asciidoc-xhtml5
asciidoc-xhtml5: $(asciidoc_targets_xhtml5)
.PHONY: asciidoc-exhtml5
asciidoc-exhtml5: $(asciidoc_targets_exhtml5)
.PHONY: asciidoc-html5s
asciidoc-html5s: $(asciidoc_targets_html5s)
.PHONY: asciidoc-cfl
asciidoc-cfl: $(asciidoc_targets_cfl)
.PHONY: asciidoc-pdf
asciidoc-pdf: $(asciidoc_targets_pdf)
.PHONY: asciidoc-db5
asciidoc-db5: $(asciidoc_targets_db5)
.PHONY: asciidoc-md
asciidoc-md: $(asciidoc_targets_md)
.PHONY: asciidoc
asciidoc: $(asciidoc_targets)

asciidoctor_flags= \
		--attribute git-version="$(git_version)" \
		--attribute git-date="$(git_date)" \
		--attribute git-datetime="$(git_datetime)" \
		--attribute git-subject="$(git_subject)" \
		--attribute git-commit-count="$(git_commit_count)" \
		--attribute git-local-changes="$(git_local_changes)" \
		--attribute date-today="$(date_today)" \
		--attribute year-today="$(year_today)" \
		--attribute datetime-now="$(datetime_now)" \
		--attribute source-highlighter="pygments" \


asciidoctor_html5_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute data-uri \
		--attribute png_or_svg=svg \
		--attribute imagegen_ext=svg \
		--attribute link_suffix=.html \

asciidoctor_xhtml5_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute data-uri \
		--attribute png_or_svg=svg \
		--attribute imagegen_ext=svg \
		--attribute link_suffix=.xhtml \

asciidoctor_exhtml5_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="images" \
		--attribute 'data-uri!' \
		--attribute png_or_svg=png \
		--attribute imagegen_ext=png \
		--attribute link_suffix=.emb.xht \

asciidoctor_html5s_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="images" \
		--attribute 'data-uri' \
		--attribute png_or_svg=svg \
		--attribute imagegen_ext=svg \
		--attribute link_suffix=.s.html \

asciidoctor_cfl_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="images" \
		--attribute 'data-uri' \
		--attribute png_or_svg=png \
		--attribute imagegen_ext=png \

asciidoctor_pdf_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \

asciidoctor_db5_flags=$(asciidoctor_flags) \
		--attribute imagesoutdir="$(current_makefile_dirname_abspath)/$(build_dir)/images" \
		--attribute imagesdir="images" \
		--attribute link_suffix='' \

################################################################################
# rules
################################################################################

$(asciidoc_targets) $(asciidoc_targets_md): \
	$(nwdiag_targets) \
	$(plantuml_targets) \
	$(image_targets) \

$(build_dir)/%.md: $(build_dir)/%.db5.xml
	mkdir -p $(dir $(@))
	pandoc --from=docbook --to=commonmark \
		--number-sections \
		--output=$(@) $(<)

$(build_dir)/%.db5.xml: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_db5) \
		$(asciidoctor_db5_flags) \
		--attribute png_or_svg=png \
		--attribute imagegen_ext=png \
		--out-file $(build_dir)/$(<:.adoc=.db5.xml) $(<)

$(build_dir)/%.html: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_html5) \
		$(asciidoctor_html5_flags) \
		--out-file $(@) $(<) \

$(build_dir)/%.xhtml: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_xhtml5) \
		$(asciidoctor_xhtml5_flags) \
		--out-file $(@) $(<) \

$(build_dir)/%.emb.xht: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_exhtml5) \
		$(asciidoctor_exhtml5_flags) \
		--out-file $(@) $(<) \

$(build_dir)/%.s.html: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_html5s) \
		$(asciidoctor_html5s_flags) \
		--out-file $(@) $(<) \

$(build_dir)/%.cfl.xml: %.adoc
	mkdir -p $(dir $(@))
	$(asciidoctor_cfl) \
		$(asciidoctor_cfl_flags) \
		--out-file $(@) $(<) \

$(build_dir)/%.pdf: %.adoc $(current_makefile_dirname_realpath)/pdf-styles/custom-theme.yml
	mkdir -p $(dir $(@))
	$(asciidoctor_pdf) \
		$(asciidoctor_pdf_flags) \
		--attribute pdf-stylesdir=$(current_makefile_dirname_realpath)/pdf-styles/ \
		--attribute pdf-fontsdir=$(current_makefile_dirname_realpath)/pdf-fonts/ \
		--attribute pdf-style=custom \
		--attribute pdf-page-size=A4 \
		--attribute png_or_svg=svg \
		--attribute imagegen_ext=svg \
		--out-file $(build_dir)/$(<:.adoc=.pdf) $(<)
	#$(current_makefile_dirname_realpath)/optimize-pdf $(build_dir)/$(<:.adoc=.pdf)
	#mv $(build_dir)/$(<:.adoc=-optimized.pdf) $(build_dir)/$(<:.adoc=.pdf)

#$(build_dir)/images/%.svg: images/%.mp
#	mkdir -p $(build_dir)/$$(dirname $(<))
#	cd $(dir $(<)) && \
#	metauml-exec mptopdf $(notdir $(realpath $(<)))
#	mv $(<:%.mp=%-1.pdf) $(@:%.svg=%.pdf)
#	pdfcrop $(@:%.svg=%.pdf) $(@:%.svg=%-crop.pdf)
#	#pdf2svg $(@:%.svg=%-crop.pdf) $(@) 1
#	inkscape --without-gui --export-plain-svg /dev/stdout $(@:%.svg=%-crop.pdf) \
#		| sed "s/font-family:[^;]\\+/font-family:'Open Sans','DejaVu Sans',sans-serif/g" \
#		> $(@)

$(build_dir)/images/%.svg $(build_dir)/images/%.png: images/%.nwdiag
	mkdir -p $(build_dir)/$$(dirname $(<))
	$(nwdiag) -T svg -o $(build_dir)/$(<:.nwdiag=.svg) $(<)
	inkscape --without-gui --export-png $(build_dir)/$(<:.nwdiag=.png) --export-dpi 300 $(build_dir)/$(<:.nwdiag=.svg)

$(build_dir)/images/%.svg $(build_dir)/images/%.png: images/%.plantuml
	mkdir -p $(build_dir)/$$(dirname $(<))
	$(plantuml) -tsvg -pipe >$(build_dir)/$(<:.plantuml=.svg) <$(<)
	$(plantuml) -tpng -pipe >$(build_dir)/$(<:.plantuml=.png) <$(<)

$(build_dir)/images/%.png: images/%.png
	mkdir -p $$(dirname $(@))
	cp $(<) $(@)

$(build_dir)/images/%.jpg: images/%.jpg
	mkdir -p $$(dirname $(@))
	cp $(<) $(@)

#$(build_dir)/images/%.emf: $(build_dir)/images/%.svg
#	inkscape --without-gui --export-emf $(@) $(<)

#$(build_dir)/images/%.wmf: $(build_dir)/images/%.svg
#	inkscape --without-gui --export-wmf $(@) $(<)

$(build_dir)/images/%.svg: images/%.svg
	mkdir -p $$(dirname $(@))
	#cp $(<) $(@)
	#sed 's/Helvetica/sans-serif/g' "$(<)" > "$(@)"
	#sed 's/Helvetica/OpenSans/g' "$(<)" > "$(@)"
	cp $(<) $(@)

$(build_dir)/images/%.pdf: images/%.pdf
	mkdir -p $$(dirname $(@))
	cp $(<) $(@)

$(build_dir)/images/%-pages: images/%.pdf
	page_count=$$( exiftool -s -t $(<) | gawk '( $$1 == "PageCount" ){ print $$2 }' ) || exit 1; \
	echo "$${page_count}";\
	mkdir -p $(@); \
	for page in $$( seq 1 "$${page_count}" ); do \
		pagei=$$(( $${page} - 1 )); \
		gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=$${page} -dLastPage=$${page} -sOutputFile=/dev/stdout $(<) | pdfcrop - $(@)/$${page}-crop.pdf || exit 1; \
		inkscape --without-gui --export-plain-svg /dev/stdout $(@)/$${page}-crop.pdf \
			| sed "s/font-family:[^;]\\+/font-family:'Open Sans','DejaVu Sans',sans-serif/g" \
			> $(@)/$${page}-crop.svg; \
	done

$(build_dir)/images/%-crop.svg $(build_dir)/images/%-crop.pdf: images/%.pdf
	mkdir -p $(build_dir)/$$(dirname $(<))
	pdfcrop $(<) $(build_dir)/$(<:.pdf=-crop.pdf)
	#pdf2svg $(build_dir)/$(<:.pdf=-crop.pdf) $(build_dir)/$(<:.pdf=-crop.svg) 1
	#inkscape --without-gui --export-plain-svg $(build_dir)/$(<:.pdf=-crop.svg) $(build_dir)/$(<:.pdf=-crop.pdf)
	inkscape --without-gui --export-plain-svg /dev/stdout $(build_dir)/$(<:.pdf=-crop.pdf) \
		| sed "s/font-family:[^;]\\+/font-family:'Open Sans','DejaVu Sans',sans-serif/g" \
		> $(build_dir)/$(<:.pdf=-crop.svg)

git=git
git_flags=
git_commit=$(git) $(git_flags) commit
git_commit_flags=
git_commit_msg=
git_push=$(git) $(git_flags) push
git_push_flags=

rclone_remote=
rclone_remote_path=
rclone_remote_path_backup=$(dir $(rclone_remote_path))zzz-backup-$(notdir $(rclone_remote_path))
rclone_config=config/rclone.conf
rclone_cmd=rclone
rclone_args=
rclone=$(rclone_cmd) $(rclone_args)

.PHONY: deploy-rclone
deploy-rclone:
	$(rclone) mkdir --config "$(rclone_config)" "$(rclone_remote):$(rclone_remote_path)"
	$(rclone) mkdir --config "$(rclone_config)" "$(rclone_remote):$(rclone_remote_path_backup)"
	$(rclone) -vv copy --backup-dir "$(rclone_remote):$(rclone_remote_path_backup)" --delete-before --config $(rclone_config) --ignore-size --ignore-checksum --files-from <(find $(build_dir)/ \( -name '*.pdf' -o -name '*.html' \) -type f -printf '%P\n') $(build_dir)/ "$(rclone_remote):$(rclone_remote_path)"

live-server: asciidoc-html5
	live-server $(build_dir)

remake_interval=1
remake:
	while true; do make -j $$(grep -c ^processor /proc/cpuinfo) $(remake_args); sleep $(remake_interval); done

.PHONY: clean
clean:
	{ test -d $(build_dir) && rm -vr $(build_dir); } || echo "Not deleting $(build_dir) ..."
	{ test -d .meta && rm -vr .meta; } || echo "Not deleting .meta ..."

.PHONY: FORCE
FORCE:

-include user-config.mk
-include extra-config.mk
