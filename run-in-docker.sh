#!/usr/bin/env bash

script_name="${0}"
script_dirname="$( dirname -- "${0}" )"
script_dirnamea="$(readlink -f -- "${script_dirname}")"
script_basename="$( basename -- "${0}" )"

mkdir -vp "${HOME}/.cache/oci/alpine-3-var-cache"

exec docker \
    container \
        run --rm \
        --volume "${HOME}/.cache/oci/alpine-3-var-cache:/var/cache:z" \
        --volume /etc/localtime:/etc/localtime:ro \
        --volume "${script_dirnamea}:/var/src/:rw" \
        --workdir /var/src \
        ${run_args} \
        --user "$(id -u):$(id -g)" \
        -i \
	    docker.io/iwana/asciidoctor-builder:alpine3-latest \
	    "${@}"
